﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Net;
using System.IO;
using System.Data.SQLite;

namespace Auk_la2kiev
{
    public partial class Form1 : Form
    {     
        // ОБЬЯВЛЕНИЕ видные классы и переменные
        CookieContainer SaveCo = new CookieContainer(); //куки
        RichTextBox temp = new RichTextBox(); // временное хранилище
        SQLiteConnection sql_conn;
        string account = "Temson";
        string password = "Test12345";
        string keystring = "";
        string cdata = "bkt";
        bool ERROR = false;
        int Maxlen = 30;
        int MaxDay = 14;

        public Form1()
        {
            InitializeComponent();
            panel3.Visible = true;
            Application.DoEvents();
           try
            {
               sql_conn = new SQLiteConnection("Data Source=Lots.db;Version=3;New=False;Compress=True;");
               sql_conn.Open();
              
            }
            catch { MessageBox.Show("НЕТ СОЕДИНЕНИЯ С Базой Данных!!"); }
           Viewer();
           panel3.Visible = false;
            
        }
        
        public class Lot // Разборка и сохранение в таблицу.
        {
            private string Grade = "-1";
            public string Images = "-1";
            public string Title = "-1";
            public string Desc = "-1";
            public string Zat = "-1";
            public string Price = "";
            public string Types = "-1";
            public string DateReg = "-1";
            public int Length = -1;
            public int Value = -1;
            public int Status = 1;

            RichTextBox html = new RichTextBox();
            SQLiteConnection sql_con;

            private string[] explode(string separator, string source)
            {
                // разделяем на отдельные строки
                return source.Split(new string[] { separator }, StringSplitOptions.None);
            }

            public string Cute(string Text,string Start, string End, int popravka)
            {
                // получение необходимых параметров
                RichTextBox tmp = new RichTextBox();
                tmp.Text=Text;
                int startErase = tmp.Find(Start);
                int EndErase = tmp.Find(End);
                if ((EndErase - startErase - popravka) <= 0) return " ";
                return tmp.Text.Substring(startErase+popravka, (EndErase - startErase - popravka));
                
            }

            public int Load(string HTMLtext)
            {
                html.Text = Cute(HTMLtext, "<tbody>", "</tbody>", 7);
                string[] Txt = explode("\n", html.Text);
                int st = 0; // Контроль линии
                int lm = 0; // Контроль масива
               
            
                foreach (string line in Txt)
                    {
                     ///////////////////////////////////////////////////////  
                    if (line.IndexOf("</tr>") != -1)
                        {
                            bool NLine = true;
                            try
                            {
                                sql_con = new SQLiteConnection("Data Source=Lots.db;Version=3;New=False;Compress=True;");
                                sql_con.Open();
                            }
                            catch { MessageBox.Show("НЕТ СОЕДИНЕНИЯ С Базой Данных!!"); return 0; }
                            string CommandText = "SELECT * FROM Lots WHERE Value="+Value;
                            SQLiteCommand sqCommand = new SQLiteCommand(CommandText, sql_con);
                            SQLiteDataReader sqReader = sqCommand.ExecuteReader();
                            while (sqReader.Read())
                            { NLine = false; }
                            if (NLine==true)
                            {
                                CommandText = "INSERT INTO Lots (Value,Grade,Image,Desc,Zat,Title,Type,Length,Price,DateReg,status,DateEnd) " +
                                    "VALUES (" + Value + ",'" + Grade + "','" + Images + "','" + Desc.Replace('\'', ' ') + "','" + Zat + "','" + Title.Replace('\'', ' ') + "','" + Types + "'," + Length + ",'" + Price + "','" + DateTime.Now + "',1,'-' " + ")";
                                sqCommand = new SQLiteCommand(CommandText, sql_con);
                                sqCommand.ExecuteNonQuery();
                            }
                        }
                    ///////////////////////////////////////////////////
                       if (st == 7)
                        {
                            Value = Convert.ToInt32(Cute(line,"value","'>",7));
                            st++;
                        }

                       if (st == 6) st++;

                       if (st == 5)
                        {
                            Price = Cute(line, "right", "</td>", 6);
                             st++;
                        }

                       if (st == 4)
                        {
                            Length = Convert.ToInt32(Cute(line, "right", "</td>", 6));
                            st++;
                        }

                       if (st == 3)
                        {
                            Desc = Cute(line,"<b>","</b>",3);
                            Zat = Cute(line, "<h5>", "</h5>", 4); 
                            st++;
                        }

                       if (st == 2)
                        {
                            Types = Cute(line, "icons/", "_", 6); 
                             Images = Cute(line, "='", "' title", 2);  
                             Title = Cute(line, "title", "'></", 7);
                             st++;
                        }

                       if (st == 1)
                        {
                            string gr = "NDCBAS";
                            Grade = gr[Convert.ToInt16(Cute(line, "grade_", ".gif", 6))].ToString();
                           
                             st++;
                        }
                
                       if (line.IndexOf("<tr>") != -1)
                         {
                             // Начало блока
                             st = 1;
                             lm++;
                         }

                    } // Конец форыча
                return lm;
        }
}
        // частные процедуры
        private void LOG(string text) // Система оповещений 
        {
            statusStrip1.Items[0].Text = text;
            Application.DoEvents();
        } 
        private void стартToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ERROR = false;
            // Первый пост запрос
                LOG("Подключение к серверу...");
            HttpWebRequest webreq = (HttpWebRequest)HttpWebRequest.Create("http://cp.la.kiev.ua");
            webreq.Accept = "*/*";
            webreq.Headers.Add("Accept-Charset", "windows-1251,utf-8;q=0.7,*;q=0.3");
            webreq.Headers.Add("Accept-Encoding", "gzip,deflate,sdch");
            webreq.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
            webreq.UserAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.33 (KHTML, like Gecko) Chrome/13.0.755.0 Safari/534.33";
            webreq.CookieContainer = SaveCo;
            webreq.Referer = "http://la.kiev.ua";
            // получаем ответ и сохраняем его
            try
            {
                WebResponse webresp = webreq.GetResponse();
                StreamReader stream = new StreamReader(webresp.GetResponseStream());
                stream = new StreamReader(webresp.GetResponseStream(), Encoding.GetEncoding("windows-1251"));
                temp.Text = stream.ReadToEnd();
            }
            catch 
            {
                MessageBox.Show(" Невожможно подключится к серверу ", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                ERROR = true;
                LOG("Внимание, произошла ошибка подключения к серверу");
            }
            if (ERROR != true)
            {
                // панелька капчи и загрузка её
                int Tmp_a = temp.Find("PHPSESSID");
                string Tmp_st = temp.Text.Substring(Tmp_a + 10, 26);
                try
                {
                    pictureBox1.Load("http://cp.la.kiev.ua/captha/index.php?PHPSESSID=" + Tmp_st);
                }
                catch
                {
                    MessageBox.Show(" Невожможно загрузить изображение", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    ERROR = true;
                    LOG("Внимание, произошла ошибка загрузки изображения");
                }
                if (ERROR != true)
                {
                    LOG("Введите полученную картинку");
                   // panel1.Left = Width ;
                   // panel1.Top = Height ;
                    panel1.Visible = true;
                }
            }
        } // начало пляски
        private void button2_Click(object sender, EventArgs e)
        {
            //>// Продолжаем разбор Капча - Аук
            panel1.Visible = false;
                LOG("Валидация введенного текста");
            keystring = capha.Text;
            
            string secondStepForm = "?login&account=" + account + "&password=" + password + "&keystring=" + keystring + "&cdata=" + cdata + "";
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create("http://cp.la.kiev.ua//");
            // Настраиваем параметры запроса
            request.UserAgent = "Mozilla/5.0";
            request.Method = "POST";
            request.AllowAutoRedirect = true;
            request.Referer = "http://cp.la.kiev.ua//";
            request.CookieContainer = SaveCo;
            request.ContentType = "application/x-www-form-urlencoded";
            // Преобразуем данные к соответствующую кодировку
            byte[] EncodedPostParams = Encoding.ASCII.GetBytes(secondStepForm);
            request.ContentLength = EncodedPostParams.Length;
            request.GetRequestStream().Write(EncodedPostParams,
                                             0,
                                             EncodedPostParams.Length);
            request.GetRequestStream().Close();
            // Получаем ответ
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string html = new StreamReader(response.GetResponseStream(),
                                           Encoding.GetEncoding("windows-1251")).ReadToEnd();
            temp.Text = html;
            //>// Здесь проверка на ошибки
            int Tmp_a,Tmp_b;
            Tmp_a = temp.Find("error colspan");
            Tmp_b = temp.Find("</td>");
            if (Tmp_b - Tmp_b < 7)
            {
                LOG("Проверка завершена, подготовка к загрузки текста");
                //>// Грузим страницу

                HttpWebRequest webreq = (HttpWebRequest)HttpWebRequest.Create("http://cp.la.kiev.ua/index.php?page=auction&view=buy_list");
                webreq.Accept = "*/*";
                webreq.Headers.Add("Accept-Charset", "windows-1251,utf-8;q=0.7,*;q=0.3");
                webreq.Headers.Add("Accept-Encoding", "gzip,deflate,sdch");
                webreq.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
                webreq.UserAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.33 (KHTML, like Gecko) Chrome/13.0.755.0 Safari/534.33";
                webreq.CookieContainer = SaveCo;
                webreq.Referer = "http://la.kiev.ua";
                WebResponse webresp = webreq.GetResponse();
                StreamReader stream = new StreamReader(webresp.GetResponseStream());
                stream = new StreamReader(webresp.GetResponseStream(), Encoding.GetEncoding("windows-1251"));
                temp.Text = stream.ReadToEnd();
                ////////////////Грузим Дальше//////////////////////////

                secondStepForm = "?page=auction&act=changechar&char=66241";
                request = (HttpWebRequest)WebRequest.Create("http://cp.la.kiev.ua/index.php?page=auction&view=buy_list");
                // Настраиваем параметры запроса
                request.UserAgent = "Mozilla/5.0";
                request.Method = "POST";
                request.AllowAutoRedirect = true;
                request.Referer = "http://cp.la.kiev.ua//";
                request.CookieContainer = SaveCo;
                request.ContentType = "application/x-www-form-urlencoded";
                byte[] EncodedPostParams2 = Encoding.ASCII.GetBytes(secondStepForm);
                request.ContentLength = EncodedPostParams2.Length;
                request.GetRequestStream().Write(EncodedPostParams2,
                                                 0,
                                                 EncodedPostParams2.Length);
                request.GetRequestStream().Close();
                response = (HttpWebResponse)request.GetResponse();
                html = new StreamReader(response.GetResponseStream(),
                                               Encoding.GetEncoding("windows-1251")).ReadToEnd();
                temp.Text = html;
                ///////////////Последняя хрень///////////////////
                webreq = (HttpWebRequest)HttpWebRequest.Create("http://cp.la.kiev.ua/index.php?page=auction&view=buy_list");
                webreq.Accept = "*/*";
                webreq.Headers.Add("Accept-Charset", "windows-1251,utf-8;q=0.7,*;q=0.3");
                webreq.Headers.Add("Accept-Encoding", "gzip,deflate,sdch");
                webreq.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
                webreq.UserAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.33 (KHTML, like Gecko) Chrome/13.0.755.0 Safari/534.33";
                webreq.CookieContainer = SaveCo;
                webreq.Referer = "http://la.kiev.ua";
                webresp = webreq.GetResponse();
                stream = new StreamReader(webresp.GetResponseStream());
                stream = new StreamReader(webresp.GetResponseStream(), Encoding.GetEncoding("windows-1251"));
               
                parsing(stream.ReadToEnd());
                LOG("Отображение данных");
                //////////////////////////
                Viewer();
                //////////////////////////
                LOG("База данных обновлена");
                
            }
            else LOG("ОШИБКА -"+temp.Text.Substring(Tmp_a + 21, Tmp_b-21));
        } // грузим картику
        private void parsing(string text)  // парсер 
        {
            // Парсер
            int str = 1;
            Lot lt = new Lot();
            StreamReader stream;

            string CommandText = "DELETE FROM Lots";
            SQLiteCommand sqCommand = new SQLiteCommand(CommandText, sql_conn);
            sqCommand.ExecuteNonQuery();
            int varlog;
            do
            {
                LOG("Обработка " + str.ToString() + " страницы");
                str++;
                varlog = lt.Load(text);
                if ((varlog <= 0) && (str <= 4)) varlog = 30;
                //////////////////////////////////////////////////////
                if (varlog >= 30)
                {
                    LOG("Загрузка " + str.ToString() + " страницы");
                    HttpWebRequest webreq = (HttpWebRequest)HttpWebRequest.Create("http://cp.la.kiev.ua/?page=auction&view=buy_list&show=" + str);
                    webreq.Accept = "*/*";
                    webreq.Headers.Add("Accept-Charset", "windows-1251,utf-8;q=0.7,*;q=0.3");
                    webreq.Headers.Add("Accept-Encoding", "gzip,deflate,sdch");
                    webreq.Headers.Add("Accept-Language", "ru-RU,ru;q=0.8,en-US;q=0.6,en;q=0.4");
                    webreq.UserAgent = "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/534.33 (KHTML, like Gecko) Chrome/13.0.755.0 Safari/534.33";
                    webreq.CookieContainer = SaveCo;
                    webreq.Referer = "http://cp.la.kiev.ua";

                    WebResponse webresp = webreq.GetResponse();
                    stream = new StreamReader(webresp.GetResponseStream());
                    stream = new StreamReader(webresp.GetResponseStream(), Encoding.GetEncoding("windows-1251"));
                    //////////////////////////////////////////////////////
                    text = stream.ReadToEnd();
                }
            }
            while (( varlog >= 30) && (str != Maxlen));
            
            // анализатор /////////////////////////////////////////////////
            LOG("Первичный анализ полученных данных");
            // Скачанную таблицу проверяем с со сохраненной таблицей
            
            CommandText = "SELECT * FROM Lots ";
            sqCommand = new SQLiteCommand(CommandText, sql_conn);
            SQLiteDataReader sqReader = sqCommand.ExecuteReader();

            while (sqReader.Read())
            {
                bool newLine = true;
                CommandText = "SELECT * FROM Items WHERE Value="+sqReader[0].ToString();
                sqCommand = new SQLiteCommand(CommandText, sql_conn);
                sqCommand.ExecuteNonQuery();

                SQLiteDataReader sqReader_2 = sqCommand.ExecuteReader();
                while (sqReader_2.Read())
                    {
                        
                        // если есть новая запись и прошло больше 1 дней.
                        
                        if ((DateTime.Now.Day - Convert.ToDateTime(sqReader_2[9]).Day) > 0)
                        {
                            CommandText = "UPDATE Items set Status=2 WHERE Value="+ sqReader[0].ToString();
                            sqCommand = new SQLiteCommand(CommandText, sql_conn);
                            sqCommand.ExecuteNonQuery();
                        }
                       newLine = false;
                    }

                if (newLine == true)
                {
                    
                    CommandText = "INSERT INTO Items (Value,Grade,Image,Desc,Zat,Title,Type,Length,Price,DateReg,status,DateEnd) " +
                                  "VALUES (" + sqReader[0].ToString() + ",'" + sqReader[1].ToString() + "','" + sqReader[2].ToString() + "','" + sqReader[3].ToString() + "','" + sqReader[4] + "','" + sqReader[5].ToString() + "','" + sqReader[6].ToString() + "'," + sqReader[7].ToString() + ",'" + sqReader[8].ToString().Replace(',','.') + "','" + sqReader[9].ToString() + "',1,'-' )";
                    sqCommand = new SQLiteCommand(CommandText, sql_conn);
                    sqCommand.ExecuteNonQuery();
                }
            } // Со качанной таблицы в инвертарную
            LOG("Вторичный анализ полученных данных");

            // Каждая ячейка сохр. таблиции сравнивается с со скачанной 
            CommandText = "SELECT * FROM Items ";
            sqCommand = new SQLiteCommand(CommandText, sql_conn);
            sqReader = sqCommand.ExecuteReader();

            while (sqReader.Read())
            {
                bool DFinder = true;
                string val = sqReader[0].ToString(); 
                CommandText = "SELECT * FROM Lots WHERE Value="+val;
                SQLiteCommand sqCommand_2 = new SQLiteCommand(CommandText, sql_conn);
                SQLiteDataReader sqReader_2 = sqCommand_2.ExecuteReader();

                while (sqReader_2.Read())
                {
                    DFinder = false;
                }
                if (DFinder == true)
                {
                    if ((DateTime.Now.Day - Convert.ToDateTime(sqReader[9]).Day) >= MaxDay) 
                    {
                        if (sqReader[11].ToString() == "-")
                        {
                            CommandText = "UPDATE Items set Status=4,DateEnd='" + DateTime.Now.ToString() + "' WHERE Value=" + sqReader[0].ToString();
                            sqCommand = new SQLiteCommand(CommandText, sql_conn);
                            sqCommand.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        if (sqReader[11].ToString() == "-")
                        {
                            CommandText = "UPDATE Items set Status=3,DateEnd='" + DateTime.Now.ToString() + "' WHERE Value=" + sqReader[0].ToString();
                            sqCommand = new SQLiteCommand(CommandText, sql_conn);
                            sqCommand.ExecuteNonQuery();
                        }
                    }
                }
            }
        } 
        private void Viewer() // Показываем табличку 
        {

            dataGridView1.Rows.Clear();
            string CommandText = "SELECT * FROM Items ";
            SQLiteCommand sqCommand = new SQLiteCommand(CommandText, sql_conn);
            int a = sqCommand.ExecuteNonQuery();
            SQLiteDataReader sqReader = sqCommand.ExecuteReader();
            int idx = -1;
            while (sqReader.Read())
            {
                dataGridView1.Rows.Add();
                idx++;
                for (int f = 0; f <= 11; f++)
                {
                    if (f == 2)
                    {
                        string str = Directory.GetCurrentDirectory() + "\\" + (sqReader[f].ToString()).Replace('/', '\\');
                        // проверяем
                        if (!File.Exists(str))
                        {
                            LOG("Загрузка картинки"+sqReader[3].ToString());

                            string url = "http://cp.la.kiev.ua/" + sqReader[f].ToString();
                            try
                            {
                                WebClient wk = new WebClient();
                                wk.DownloadFile(url, str);
                            }
                            catch
                            {
                                File.Copy(Directory.GetCurrentDirectory()+"\\images\\no_image.png", str);
                            }
                          
                        }

                        Bitmap bm = new Bitmap(str);
                        dataGridView1.Rows[idx].Cells[f].Value = bm;
                        LOG("Продолжение отображения");
                  
                        continue;
                    }

                    if (f == 9)
                    {
                        // Погадать дату
                        dataGridView1.Rows[idx].Cells[f+1].Value = sqReader[f].ToString();
                        continue;
                    }

                    if (f == 10)
                    {
                        int ind = Convert.ToInt16(sqReader[f].ToString());
                        if (ind == 1)
                        {
                            dataGridView1.Rows[idx].DefaultCellStyle.BackColor = Color.DeepSkyBlue;
                            dataGridView1.Rows[idx].Cells[f-1].Value = "Новый Лот";
                        }
                        if (ind == 2)
                        {
                            dataGridView1.Rows[idx].DefaultCellStyle.BackColor = Color.White;
                            dataGridView1.Rows[idx].Cells[f - 1].Value = "На продаже";
                        }
                        if (ind == 3)
                        {
                            dataGridView1.Rows[idx].DefaultCellStyle.BackColor = Color.LimeGreen;
                            dataGridView1.Rows[idx].Cells[f - 1].Value = "Продано";
                        }
                        if (ind == 4)
                        {
                            dataGridView1.Rows[idx].DefaultCellStyle.BackColor = Color.Coral;
                            dataGridView1.Rows[idx].Cells[f - 1].Value = "Снят";
                        }
                        continue;
                    }
                    dataGridView1.Rows[idx].Cells[f].Value = sqReader[f];
                } LOG("Показ текста");
            }
        }
        private void parsingToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // Здесь должен быть анализ сколько осталось страниц и их контроль.
            Lot lt = new Lot();
            temp.LoadFile("home.txt", RichTextBoxStreamType.PlainText);
            Text = lt.Load(temp.Text).ToString();
              
        }
        private void помощьToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void выходToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void оПрограммеToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Aboid ab = new Aboid();
            
            ab.Show();

        }

        

        
    }
}
